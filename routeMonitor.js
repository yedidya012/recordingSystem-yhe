/**
 * Created by אביה on 20 אוקטובר 2018.
 */

var logger = require('./modules/logger');
var monitor = require('./modules/monitor');

function routeMonitor(router) {
    router.get('/monitor', function (req, res) {
        res.redirect('/monitor.html');
    });

    router.get('/api/monitor/nonMp3', function (req, res) {
        logger.debug('Accessed /api/monitor/nonMp3');
        sendPromiseAndHandleErrors(res, monitor.findNonMp3Files(req.query.path));
    });

    router.get('/api/monitor/duplicates', function (req, res) {
        logger.debug('Accessed /api/monitor/duplicates');
        sendPromiseAndHandleErrors(res, monitor.findDuplicatedFiles(req.query.path));
    });

    router.get('/api/monitor/differentArtist', function (req, res) {
        logger.debug('Accessed /api/monitor/differentArtist');
        sendPromiseAndHandleErrors(res, monitor.findWithDifferentArtist(req.query.path));
    });

    router.get('/api/monitor/outOfOrder', function (req, res) {
        logger.debug('Accessed /api/monitor/outOfOrder');
        sendPromiseAndHandleErrors(res, monitor.findOutOfOrder(req.query.path));
    });

    router.get('/api/monitor/inactive', function (req, res) {
        logger.debug('Accessed /api/monitor/inactive');
        sendPromiseAndHandleErrors(res, monitor.findInactive(req.query.path));
    });
    
    router.get('/api/monitor/getYears', function (req, res) {
        logger.debug('Accessed /api/monitor/getYears');
        sendPromiseAndHandleErrors(res, monitor.getYears());
    });
}

function sendPromiseAndHandleErrors(res, promise) {
    promise.then(function (files) {
        res.send(files);
    })
    .fail(function (error) {
        logger.error('Error! Error message: ' + error.message);
        res.sendStatus(500);
    })
    .done();
}

module.exports = routeMonitor;