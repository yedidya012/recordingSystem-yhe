/**
 * Created by מרדכי on 30 יוני 2016.
 */

var secretConfig = require('./secretConfig');

const year = '5779'

var configuration = {
    server: {
        old_port: 8000,
        new_port: 80
    },
    uploadsFolder: 'test_data/' + year,
    logger: {
        infoLogFileName: 'logs/infoLog.log',
        debugLogFileName: 'logs/debugLog.log'
    },
    dirScanner: {
        outFile: 'public/jstreedata.js',
        showTopLevel: false,
        refreshDelay: 3000
    },
    metadata: {
        applyModule: true,
        minimumTrackNumber: 1,
        currentYear: year
    },
    uploadToAmazon: {
        applyModule: true,
        accessKeyId: secretConfig.accessKeyId,
        secretAccessKey: secretConfig.secretAccessKey,
        region: 'eu-central-1',
        prefix: year+'/'
    },
    monitor: {
        mainArtistPercent: 50,
        inactive: {
            stopTrackingInactiveAfter: 150, //Defines how much time before we give up on alerts (e.g previous years' lessons)
            freqs: {    //Defines the types of classes, that's also the timeout
                day: 5,
                week: 10,
                changing: 20
            }
        }
    }
};

module.exports = configuration;