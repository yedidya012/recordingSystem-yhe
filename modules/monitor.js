/**
 * Created by אביה on 23 אוגוסט 2018.
 */
var q = require('q');
var fs = require('fs');
var fsPath = require('path');
var config = require('../config');

var findNonMp3 = require('./anomalies/non-mp3');
var findDuplicates = require('./anomalies/duplicates');
var findOutOfOrder = require('./anomalies/out-of-order');
var findWithDifferentArtist = require('./anomalies/with-different-artist');
var findInactive = require('./anomalies/inactive');

function isEmpty(files) {
    return files === undefined || files === null || files.length != null && files.length === 0;
}

function getFiles(dir) {
    return q.ninvoke(fs, 'readdir', dir).then(function (files) {
        if (isEmpty(files))
            return [];

        return q.all(files.map(function (file) {
            var file = fsPath.join(dir, file);
            return q.ninvoke(fs, 'lstat', file).then(function (stat) {

                if (stat.isDirectory()) {
                    return getFiles(file);
                } else {
                    return [file];
                }
            });
        }));
    }).then(function (files) {
        if (isEmpty(files))
            return [];

        return files.reduce(function (pre, cur) {
            return pre.concat(cur);
        });
    });
}

function forgePath(pathInMaagar) {
    if (pathInMaagar === undefined)
        return undefined;

    const root = fsPath.dirname(config.uploadsFolder);
    const finalPath = fsPath.join(root, pathInMaagar);

    function isDescendantOf(parent, dir) {
        const relative = fsPath.relative(parent, dir);
        return !relative.startsWith('..') && !fsPath.isAbsolute(relative);
    }

    if (isDescendantOf(root, finalPath))
        return finalPath;

    return undefined;
}

function findAnomalies(processor) {
    return function (pathInMaagar) {
        const finalPath = forgePath(pathInMaagar);

        if (finalPath === undefined)
            return q.fcall(function () {
                throw new Error('invalid path: ' + pathInMaagar);
            });

        return getFiles(finalPath)
                .then(files => q.fcall(processor, files));
    };
}

function getYears() {
    const root = fsPath.dirname(config.uploadsFolder);

    return q.ninvoke(fs, 'readdir', root)
        .then(function (files) {
            var years = files.filter(name =>
                name.match(/5\d\d\d/)
                && fs.statSync(fsPath.join(root, name)).isDirectory());
            var currentYear = config.metadata.currentYear;

            return { years: years, currentYear: currentYear };
        });
}

module.exports.findNonMp3Files = findAnomalies(findNonMp3);
module.exports.findDuplicatedFiles = findAnomalies(findDuplicates);
module.exports.findOutOfOrder = findAnomalies(findOutOfOrder);
module.exports.findWithDifferentArtist = findAnomalies(findWithDifferentArtist);
module.exports.findInactive = findAnomalies(findInactive);

module.exports.getYears = getYears;