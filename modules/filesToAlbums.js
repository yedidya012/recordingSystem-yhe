/**
 * Created by אביה on 23 אוגוסט 2018.
 */
var fsPath = require('path');

function toAlbums(files) {
    const albumDict = {};

    for (f of files) {
        album = fsPath.dirname(f);

        albumDict[album] = albumDict[album] || [];
        albumDict[album].push(f);
    }

    return Object.keys(albumDict).map(function (key) {
        return { name: key, files: albumDict[key] };
    });
}

module.exports = toAlbums;