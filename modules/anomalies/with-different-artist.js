/**
 * Created by אביה on 23 אוגוסט 2018.
 */
var q = require('q');
var ffmetadata = require('ffmetadata');
var config = require('../../config');

var toAlbums = require('../filesToAlbums');

function isEmpty(files) {
    return files === undefined || files === null || files.length != null && files.length === 0;
}

function FindWithDifferentArtist(files) {
    const albums = toAlbums(files);

    const readMetadata = q.nfbind(ffmetadata.read);

    function promiseFileWithArtist(file) {
        return readMetadata(file)
            .then(metadata => { return { 'path': file, 'artist': metadata.artist } })
    };

    function findMainArtist(artists) {
        if (artists.length == 0)
            return null;

        var modeMap = {};
        var maxEl = artists[0], maxCount = 1;
        for (var i = 0; i < artists.length; i++) {
            var el = artists[i];
            if (modeMap[el] == null)
                modeMap[el] = 1;
            else
                modeMap[el]++;
            if (modeMap[el] > maxCount) {
                maxEl = el;
                maxCount = modeMap[el];
            }
        }

        if (maxCount > artists.length * config.monitor.mainArtistPercent / 100)
            return maxEl;

        return null;
    };

    function doAlbum(albumFiles) {
        if (isEmpty(albumFiles))
            return [];

        return q.all(albumFiles.map(f => promiseFileWithArtist(f)))
                .then(function (filesWithArtist) {
                    mainArtist = findMainArtist(filesWithArtist.map(a => a.artist));

                    if (mainArtist)
                        return filesWithArtist
                            .filter(a => a.artist !== mainArtist)
                            .map(a => {
                                return { 'file': a.path, 'expected': mainArtist, 'found': a.artist || 'None' }
                            });
                    return [];
                });
    };

    if (isEmpty(albums))
        return [];

    return q.all(albums.map(album => doAlbum(album.files)))
            .then(result => result.reduce((a, b) => a.concat(b)));
}

module.exports = FindWithDifferentArtist;