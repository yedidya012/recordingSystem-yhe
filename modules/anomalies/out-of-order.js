/**
 * Created by אביה on 23 אוגוסט 2018.
 */
var fsPath = require('path');

var toAlbums = require('../filesToAlbums');

function isEmpty(files) {
    return files === undefined || files === null || files.length != null && files.length === 0;
}

function FindOutOfOrder(files) {
    return toAlbums(files)
            .map(function (album) {
                var counter = 0;

                const filesOutOfOrder = album.files.filter(function (file) {
                    const name = fsPath.basename(file);
                    counter = counter + 1;
                    const expected = ('0' + counter).slice(-2);
                    const actual = name.slice(0, 2);

                    return expected !== actual || name[2] !== '.';
                });

                return { album: album.name, files: filesOutOfOrder };
            })
            .filter(outOfOrderAlbum => !isEmpty(outOfOrderAlbum.files));
}

module.exports = FindOutOfOrder;