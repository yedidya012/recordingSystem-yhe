/**
 * Created by אביה on 23 אוגוסט 2018.
 */

var crypto = require('crypto');
var fs = require('fs');
var sha1File = require('sha1-file');
var readChunk = require('read-chunk');

function FindDuplicates(files) {
    const filesAttr = {};
    const filesBy = {};

    function calcAttr(file, attr, getAttr) {
        if (file in filesAttr && attr in filesAttr[file])
            return filesAttr[file][attr];

        const attrValue = getAttr();

        if (!(attr in filesBy))
            filesBy[attr] = {};

        if (!(attrValue in filesBy[attr]))
            filesBy[attr][attrValue] = [];

        if (!(file in filesAttr))
            filesAttr[file] = {};

        filesBy[attr][attrValue].push(file);
        filesAttr[file][attr] = attrValue;

        return attrValue;
    };

    function calcSize(file) {
        return calcAttr(file, "size", function () {
            return fs.statSync(file).size;
        });
    };

    function calcSH(file) {
        return calcAttr(file, "sh", function () {
            return crypto
                .createHash('sha1')
                .update(readChunk.sync(file, 0, 1024))
                .digest('hex');
        });
    };
    function calcHash(file) {
        return calcAttr(file, "hash", function () {
            return sha1File(file);
        });
    };

    function addFile(file) {
        const size = calcSize(file);
        if (filesBy["size"][size].length > 1)
            for (f of filesBy["size"][size]) {
                sh = calcSH(f);

                if (filesBy["sh"][sh].length > 1)
                    for (f2 of filesBy["sh"][sh]) {
                        hash = calcHash(f2);
                    }
            }
    }

    files.forEach(f => addFile(f));

    if (!filesBy["hash"])
        return [];

    return Object.keys(filesBy["hash"]).map(k => filesBy["hash"][k]);
}

module.exports = FindDuplicates;