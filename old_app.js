/**
 * Created by מרדכי on 29 יוני 2016.
 */

var config = require('./config');
var logger = require('./modules/logger');
var express = require('express');
var os = require('os')

module.exports.execute = function () {
    
    var old_app = express();
    var redirect = 'http://' + os.hostname();
    if (config.server.new_port !== 80)
        redirect += ':' + config.server.new_port;

    old_app.get('/', function (req, res) {
        res.redirect(301, redirect);
    });
    old_app.listen(config.server.old_port, function () {
        logger.info('Backward Server is listening on port ' + config.server.old_port);
    });
};